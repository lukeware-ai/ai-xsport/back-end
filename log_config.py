import logging

class ColoredFormatter(logging.Formatter):
    COLORS = {
        logging.DEBUG: "\033[94m",   # Azul
        logging.INFO: "\033[92m",    # Verde
        logging.WARNING: "\033[93m", # Amarelo
        logging.ERROR: "\033[91m",   # Vermelho
        logging.CRITICAL: "\033[91m" + "\033[1m",  # Vermelho brilhante
    }

    def format(self, record):
        log_color = self.COLORS.get(record.levelno, "")
        reset_color = "\033[0m"
        record.msg = f"{log_color}{record.msg}{reset_color}"
        return super().format(record)

def setup_logger(name=None, level=logging.WARNING):
    logger = logging.getLogger(name)
    logger.setLevel(level)

    if not logger.hasHandlers():
        console_handler = logging.StreamHandler()
        console_handler.setLevel(level)
        console_handler.setFormatter(ColoredFormatter('%(asctime)s - %(levelname)s - %(message)s'))
        logger.addHandler(console_handler)

    return logger

# Configura o logger principal
setup_logger()
