from ai.steps.clean_dataset import CleanDataSet
from bot_web.utils.file_util import FileUtils

if __name__ == '__main__':
    FileUtils.import_data(origin="./dataset", destination="./dataset_football", filename="dataset")
    CleanDataSet.initialize(folder_in="./dataset_football/dataset.parquet", folder_out="./dataset_football", is_save=True)