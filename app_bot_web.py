import argparse

from bot_web.bot import MainBot


def str2bool(v):
  if isinstance(v, bool):
    return v
  if v.lower() in ('yes', 'true', 't', 'y', '1'):
    return True
  elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    return False
  else:
    raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--just-download', type=str2bool,
                      help='Do you just want to download the links already downloaded?')
  parser.add_argument('--dt-start', type=str, required=False, help='Enter the start date "2024-06-27"')
  parser.add_argument('--dt-end', type=str, required=False, help='Enter the end date "2024-06-27"')
  parser.add_argument('--qt-thread', type=int, required=False, help='Enter the thread ')

  args = parser.parse_args()

  MainBot.init(dt_start=args.dt_start, dt_end=args.dt_end, just_download=args.just_download, qt_thread=args.qt_thread)
  #MainBot.init(dt_start="2024-06-15", dt_end="2024-07-30")
