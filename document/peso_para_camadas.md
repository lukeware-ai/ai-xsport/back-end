Em aprendizado de máquina com redes neurais usando TensorFlow, os pesos (weights) para cada característica são definidos automaticamente durante o treinamento do modelo. Eles são inicializados de forma aleatória e ajustados através do processo de backpropagation para minimizar a função de perda. No entanto, você pode definir a inicialização dos pesos e verificar como eles evoluem durante o treinamento.

Aqui está um exemplo de como você pode definir uma rede neural simples usando TensorFlow e Keras, e como os pesos são manipulados:

```python
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

# Definindo o modelo
model = Sequential([
    Dense(10, input_shape=(5,), activation='relu', kernel_initializer='he_normal'),  # Camada oculta com 10 neurônios
    Dense(1, activation='sigmoid')  # Camada de saída com 1 neurônio
])

# Compilando o modelo
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Visualizando a arquitetura do modelo
model.summary()
```

### Inicialização dos Pesos
Os pesos na camada densa são inicializados usando a função `kernel_initializer`. No exemplo acima, `he_normal` é usado para a inicialização dos pesos. Existem várias estratégias de inicialização de pesos, como `glorot_uniform`, `he_uniform`, `lecun_normal`, entre outras.

### Visualizando Pesos
Você pode visualizar os pesos de uma camada específica após a inicialização ou após o treinamento:

```python
# Obter os pesos da primeira camada
weights, biases = model.layers[0].get_weights()

print("Pesos iniciais da primeira camada:", weights)
print("Biases iniciais da primeira camada:", biases)
```

### Treinamento do Modelo
Os pesos serão ajustados automaticamente durante o treinamento:

```python
# Dados fictícios para treinamento
import numpy as np
X_train = np.random.random((100, 5))  # 100 amostras, 5 características
y_train = np.random.randint(2, size=(100, 1))  # 100 rótulos binários

# Treinamento
model.fit(X_train, y_train, epochs=10, batch_size=10)

# Visualizar os pesos após o treinamento
weights, biases = model.layers[0].get_weights()
print("Pesos após o treinamento da primeira camada:", weights)
print("Biases após o treinamento da primeira camada:", biases)
```

### Ajuste Manual dos Pesos (Opcional)
Se você precisar definir manualmente os pesos, isso pode ser feito após a criação do modelo, mas antes do treinamento. Aqui está um exemplo de como definir manualmente os pesos de uma camada:

```python
# Definindo pesos e biases manualmente
new_weights = np.random.rand(5, 10)  # Novos pesos aleatórios (correspondendo à camada com 5 entradas e 10 neurônios)
new_biases = np.random.rand(10)      # Novos biases aleatórios

# Definindo os pesos manualmente
model.layers[0].set_weights([new_weights, new_biases])

# Verificando se os pesos foram atualizados
weights, biases = model.layers[0].get_weights()
print("Novos pesos da primeira camada:", weights)
print("Novos biases da primeira camada:", biases)
```

### Resumo
- **Inicialização dos Pesos**: Usada na definição das camadas.
- **Visualização dos Pesos**: Pode ser feita a qualquer momento usando `get_weights()`.
- **Ajuste dos Pesos**: Feito automaticamente durante o treinamento através do backpropagation.
- **Definição Manual dos Pesos**: Opcionalmente, pode ser feita usando `set_weights()`. 

Os pesos são ajustados para minimizar a função de perda durante o treinamento, permitindo que a rede neural aprenda as melhores representações das características do conjunto de dados.