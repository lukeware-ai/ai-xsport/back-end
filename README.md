# back-end

## Comando TensorFlow

```shell
tensorboard --logdir logs/fit
```

## tasks

- desenvolver serviços api rest
    - serviço que busca dados do jogos entre datas
    - serviço que processa a AI e cria um modelo
    - serviço predizer uma partida entre dois times

- implementar no preprocessamento a correlação das feature com o resultado.

```python
for i in df.columns:
  if i != 'rs':
    fig = px.scatter(df, x='rs', y=i, title=f'Actual vs {i}')
    fig.update_layout(
      xaxis_title='Previsão de jogo',
      yaxis_title=i
    )
    fig.show()
#  ou 
import seaborn as sns
import plotly.express as px

sns.pairplot(df)
plt.show()

# ou
df.hist(bins=20, figsize=(10, 10), color='g')
plt.show()
```

- aplicar tecnica de validação cruzada na AI
- aplicar (validação cruzada )k-fold cross validation n=3 a 10 vesez (stratified k-fold cross validation) 77%(treino)
  sobreviven, 23% morre(test)

```python
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict, StratifiedKFold,
  StratifiedShuffleSplit, RepeatedKFold
from sklearn import metrics

for i in range(3, 11):
  print(f"\n----------------------------cv = {i}----------------------------------\n")
  scores_cv = cross_val_score(rf_classifier, data, labels, cv=i)
  print(scores_cv)
  print(f"Acurácia: %{scores_cv.mean():.2f} (+/-) %{scores_cv.std() * 2:.2f}\n")

# ou 
cv_strat = StratifiedKFold(n_splits=3)
scores_cv = cross_val_score(rf_classifier, data, labels, cv=cv_strat)
print(scores_cv)
print(f"Acurácia: %{scores_cv.mean():.2f} (+/-) %{scores_cv.std() * 2:.2f}\n")
# ou 
rkf = RepeatedKFold(n_splits=3, n_repeats=5, random_state=0)
scores_cv = cross_val_score(rf_classifier, data, labels, cv=rkf)
print(scores_cv)
print(f"Acurácia: %{scores_cv.mean():.2f} (+/-) %{scores_cv.std() * 2:.2f}\n")
# usar isso tambem testar isso scoring=f1
f1 = metrics.make_scorer(metrics.f1_score)
# teste.py isso RandomizedSearchCV evitar usar o GridSearchCV
from sklearn.model_selection import RandomizedSearchCV

```

- enternder melhor sobre esse modelo
  ![alt text](image.png)