import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler

from .clean_dataset import CleanDataSet


class Normalization:
  scaler = StandardScaler()
  encoder = LabelEncoder()

  @staticmethod
  def normalize(df_date: pd.DataFrame, column_label) -> tuple:
    df_numbers = df_date.select_dtypes(include=[np.number])
    features_scaled = Normalization.scaler.fit_transform(df_numbers.values)
    Normalization.encoder.fit(df_date['rs'].unique())
    df_date['rs_encoder'] = Normalization.encoder.transform(df_date['rs'])
    labels_encoder = df_date['rs_encoder']

    return features_scaled, labels_encoder, Normalization.scaler, Normalization.encoder


if __name__ == '__main__':
  df = CleanDataSet.initialize(folder_in="../../dataset/dataset.csv_bkp_2024_07_04")
  Normalization.normalize(df, column_label="rs")
