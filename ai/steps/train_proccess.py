from datetime import datetime

import joblib
import numpy as np
import tensorflow.keras as keras

from .train_preparation import TrainPreparation


class TrainProcess:
  @staticmethod
  def _build_model(length: int, learning_rate=0.001, activation='swish', units=192, output_units=3):
    model = keras.Sequential([
      keras.Input(shape=(length,)),
      keras.layers.Dense(units, activation=activation),
      keras.layers.LayerNormalization(),
      keras.layers.Dropout(0.2),
      keras.layers.Dense(units, activation=activation),
      keras.layers.LayerNormalization(),
      keras.layers.Dropout(0.2),
      keras.layers.Dense(units, activation=activation),
      keras.layers.LayerNormalization(),
      keras.layers.Dropout(0.2),
      keras.layers.Dense(units, activation=activation),
      keras.layers.LayerNormalization(),
      keras.layers.Dropout(0.2),
      keras.layers.Dense(16, activation=activation),
      keras.layers.Dropout(0.2),
      keras.layers.Dense(output_units, activation="softmax"),
    ])
    optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimizer, loss='mse', metrics=['mae', 'accuracy'])
    return model

  @staticmethod
  def train_process(folder_dataset: str = "./dataset/dataset_mined.parquet",
                    folder_model: str = "./model",
                    folder_scaler: str = './model',
                    folder_encoder="./model",
                    epochs: int = 320,
                    batch_size: int = 0
                    ):
    prepare = TrainPreparation.prepare(folder_dataset)
    train_features = prepare.get("train_features")
    train_labels = prepare.get("train_labels")
    test_features = prepare.get("test_features")
    test_labels = prepare.get("test_labels")
    scaler = prepare.get("scaler")
    encoder = prepare.get("encoder")

    num_classes = np.unique(train_labels).size
    train_labels_category = keras.utils.to_categorical(train_labels, num_classes=num_classes)
    test_labels_category = keras.utils.to_categorical(test_labels, num_classes=num_classes)

    model = TrainProcess._build_model(train_features.shape[1])
    log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d_%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    model.fit(train_features,
              train_labels_category,
              epochs=epochs,
              batch_size=batch_size,
              verbose=1,
              validation_data=(test_features, test_labels_category),
              callbacks=[tensorboard_callback]
              )

    model.save(f"{folder_model}/model.keras")
    joblib.dump(scaler, f"{folder_scaler}/scaler.pkl")
    joblib.dump(encoder, f"{folder_encoder}/encoder.pkl")
