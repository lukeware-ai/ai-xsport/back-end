import pandas as pd
from sklearn.model_selection import (train_test_split)

from .normalization import Normalization


class TrainPreparation:

  @staticmethod
  def prepare(folder_dataset: str) -> dict:
    if ".parquet" in folder_dataset:
      df = pd.read_parquet(folder_dataset)
    else:
      df = pd.read_csv(folder_dataset)

    df = df.drop(columns=['tl_0', 'tv_0', 'tl_1', 'tv_1'], errors="ignore")
    features, labels, scaler, encoder = Normalization.normalize(df_date=df, column_label="rs")
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size=0.30,
                                                                                random_state=0, stratify=labels)
    return {
      "df": df,
      "features": features,
      "labels": labels,
      "train_features": train_features,
      "train_labels": train_labels,
      "test_features": test_features,
      "test_labels": test_labels,
      "encoder": encoder,
      "scaler": scaler,
    }
