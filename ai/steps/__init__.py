from .clean_dataset import CleanDataSet
from .normalization import Normalization
from .train_preparation import TrainPreparation
from .train_proccess import TrainProcess
