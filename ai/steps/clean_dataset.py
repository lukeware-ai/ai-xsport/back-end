from numpy import double
import pandas as pd

from ai.utils.convert_date import ConvertDate
from ai.utils.inconsistency_values import InconsistencyValues
from ai.utils.is_null_or_nan import IsNullOrNaN

class CleanDataSet:
  
  @classmethod
  def initialize(cls, folder_in: str, folder_out: str = None, is_save: bool = False) -> pd.DataFrame:
    pd.options.mode.copy_on_write = True
    cls.instance = CleanDataSet()
    cls.instance._load_dataset(folder_in)
    cls.instance.df = ConvertDate.convert_date(cls.instance.df, "date")
    cls.instance.df = IsNullOrNaN.is_null_or_nan(cls.instance.df)
    if is_save:
      cls.instance.df.to_csv(f"{folder_out}/dataset_mined.csv", index=False)
      cls.instance.df.to_parquet(f"{folder_out}/dataset_mined.parquet", index=False)

    cls.instance.df = cls.instance.df.sort_values(by=['year', 'month', 'day'])
    return cls.instance.df
  

  @classmethod
  def _add_default_value(cls, key:str, default_value:any, dtype:type):
    cls.instance.df[key] = cls.instance.df[key].astype(dtype)
    cls.instance.df[key] = cls.instance.df[key].fillna(default_value)
    return cls.instance.df


  def _load_dataset(self, filename: str) -> None:
    if ".parquet" in filename:
      self.df = pd.read_parquet(filename)
    else:
      self.df = pd.read_csv(filename)
