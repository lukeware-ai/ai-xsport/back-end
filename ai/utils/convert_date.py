import pandas as pd


class ConvertDate:

  @staticmethod
  def convert_date(df: pd.DataFrame, column: str) -> pd.DataFrame:
    df_date = pd.to_datetime(df[column])
    df["month"] = df_date.dt.month
    df["day"] = df_date.dt.day
    df["year"] = df_date.dt.year
    df["weekday"] = df_date.dt.weekday
    df = df.drop(columns=[column])

    return df
