import pandas as pd


class IsNullOrNaN:
  @staticmethod
  def is_null_or_nan(df: pd.DataFrame) -> pd.DataFrame:
    numeric_cols = df.select_dtypes(include=['number']).columns
    df[numeric_cols] = df[numeric_cols].fillna(0.0)
    df[numeric_cols] = df[numeric_cols].replace(-0.0, 0.0)
    return df
