import pandas as pd


class InconsistencyValues:
    @staticmethod
    def adjust_inconsistencies(df: pd.DataFrame, variation: float = 3) -> pd.DataFrame:
        numeric_cols = df.select_dtypes(include=['number']).columns
        medians = df[numeric_cols].median()

        adjusted_df = df.copy()
        for col in numeric_cols:
            col_median = medians[col]
            mask = adjusted_df[col] > col_median * variation
            adjusted_df[col] = adjusted_df[col].where(~mask, col_median)

            mask = adjusted_df[col] * variation < col_median
            adjusted_df[col] = adjusted_df[col].where(~mask, col_median)

        return adjusted_df
