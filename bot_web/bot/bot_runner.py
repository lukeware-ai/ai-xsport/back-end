import concurrent.futures  # Necessário apenas no Windows
import re
import time
from datetime import date
from datetime import datetime
from functools import partial
from pathlib import Path

from tqdm import tqdm

from bot_web.bot.process_team_data import ProcessTeamData
from bot_web.utils import FileUtils
from bot_web.utils import LinksUtil
from log_config import setup_logger

logger = setup_logger(__name__)


class BotRunner:
  MAX_THREADS = 4

  @staticmethod
  def run(links: list[str] = None, just_download: bool = False, qt_thread: int = 4) -> None:
    BotRunner.MAX_THREADS = qt_thread
    if not just_download:
      BotRunner._search_sub_links(links)

    BotRunner._process_links()
    return None

  @staticmethod
  def _search_sub_links(links: list[str]) -> None:
    with tqdm(total=len(links), desc="processing the search for new sub-links") as pbar:
      with concurrent.futures.ThreadPoolExecutor(max_workers=BotRunner.MAX_THREADS) as executor:
        futures = [executor.submit(LinksUtil.search_links, link) for link in links]

        for future in concurrent.futures.as_completed(futures):
          try:
            future.result()
            pbar.update(1)
          except Exception as e:
            logger.error(f"\nAn exception error occurred while loading the sub-links: {e}")

  @staticmethod
  def _process_links() -> None:
    files: list[str] = FileUtils.get_files_txt("./links")
    list(map(lambda file: BotRunner._download_data(file), files))

  @staticmethod
  def _download_data(output_file: str) -> None:
    try:
      match = re.search(r"(\d{4}_\d{2}_\d{2})", str(output_file))
      dt = date.today()
      if match:
        date_str = match.group(1)
        dt = datetime.strptime(date_str, "%Y_%m_%d").date()
      else:
        print("Data não encontrada na string")

      with open(output_file, "r") as file:
        links = [link.strip() for link in file.readlines()]

      with concurrent.futures.ThreadPoolExecutor(max_workers=BotRunner.MAX_THREADS) as executor:
        with tqdm(total=len(links), desc="downloading...") as pbar:
          futures = {
            executor.submit(partial(BotRunner._process_link), link, dt, pbar, output_file): link
            for link in links
          }

          for future in concurrent.futures.as_completed(futures):
            try:
              future.result()
            except Exception as e:
              logger.error(f"exception occurred during link processing: {e}")

    except FileNotFoundError:
      logger.error(f"file '{output_file}' not found.")
    except Exception as e:
      logger.error(f"an error occurred while processing the file '{output_file}': {e}")

  @staticmethod
  def _process_link(link: str, dt: date, pbar, output_file: str) -> None:
    try:
      if link:
        with ProcessTeamData(url=link, output_folder="./dataset", date=dt) as process:
          logger.warning(f"\nprocessing file: {output_file}")
          logger.warning(f"\nlink: {link}")
          start = time.time()
          process.run(pbar)
          FileUtils.delete(output_file)
          end = time.time()
          logger.warning(f"\nExecution Time: {end - start} secounds")
    except Exception as e:
      logger.error(f"an exception error occurred while getting data from the \nlink '{link}': \n{e}")
      Path("./errors").mkdir(parents=True, exist_ok=True)
      with open("./errors/log.txt", "a") as f:
        f.write(link + "\n")


if __name__ == "__main__":
  files: list[str] = FileUtils.get_files_txt("../../errors")
  for output_file in files:
    with open(output_file, "r") as file:
      links = [link.strip() for link in file.readlines()]

    for link in links:
      BotRunner._process_link(link=link, dt=date.today(), pbar=None)
