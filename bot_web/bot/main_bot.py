from datetime import datetime, timedelta

from bot_web.bot.bot_runner import BotRunner


class MainBot:
  _DOMAIN = "https://www.sofascore.com/pt/futebol"

  @staticmethod
  def init(dt_start: str, dt_end: str, just_download: bool = False, qt_thread: int = 4) -> None:
    if not just_download:
      dt_start = datetime.strptime(dt_start, "%Y-%m-%d").date()
      dt_end = datetime.strptime(dt_end, "%Y-%m-%d").date()
      links: list[str] = []
      current_date = dt_start
      while current_date <= dt_end:
        links.append(f'{MainBot._DOMAIN}/{current_date.strftime("%Y-%m-%d")}')
        current_date += timedelta(days=1)

      BotRunner.run(links=links, qt_thread=qt_thread)
    else:
      BotRunner.run(just_download=just_download)

    return None
