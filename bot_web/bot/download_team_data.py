import pandas as pd

from bot_web.utils.text_util import TextUtil
from bot_web.utils.web_driver_util import WebDriverUtil
from log_config import setup_logger

logger = setup_logger(__name__)


class DownloadTeamData:

  def __init__(self, url: str):
    self.url = url
    self.web_driver_util: WebDriverUtil = WebDriverUtil(url=url)
    self.prefix = None
    self._keys_opinion = None
    self._keys_general_data_negative = None

  def configure(self, prefix=None):
    self.prefix = prefix
    self._keys_opinion = [
      f"{self.prefix}_vitoria_opniao",
      f"{self.prefix}_empate_opniao",
      f"{self.prefix}_perde_opniao"
    ]
    self._keys_general_data_negative = [
      f"{self.prefix}_defesas_do_goleiro",
      f"{self.prefix}_faltas",
      f"{self.prefix}_cartoes_amarelos",
      f"{self.prefix}_chutes_bloqueados",
      f"{self.prefix}_grandes_chances_perdidas",
      f"{self.prefix}_impedimentos",
      f"{self.prefix}_desarmes_sofridos",
      f"{self.prefix}_cartoes_amarelos",
      f"{self.prefix}_cartoes_vermelhos",
      f"{self.prefix}_prob_1",
      f"{self.prefix}_socos",
    ]

  def download(self) -> pd.DataFrame:
    tl: dict = self._download_tl()
    tv: dict = self._download_tv()
    if (tl and len(tl.keys()) > 10) and (tv and len(tv.keys()) > 10):
      data_frame = pd.concat([pd.DataFrame([tl]), pd.DataFrame([tv])], axis=1)
      data_frame['resultado'] = data_frame.apply(DownloadTeamData._determine_result, axis=1)
      return data_frame
    return pd.DataFrame()

  @staticmethod
  def _determine_result(row) -> str:
    if row['tl_gols'] > row['tv_gols']:
      return 'vitoria_time_local'
    elif row['tl_gols'] < row['tv_gols']:
      return 'vitoria_time_visitante'
    else:
      return 'empate'

  def _download_tl(self):
    self.configure("tl")
    return self._load_data()

  def _download_tv(self):
    self.configure("tv")
    return self._load_data()

  def _load_data(self) -> dict:
    self.data = {}
    self._name()
    self._gols()
    self._odds()
    self._opinion()
    self._general_data()
    self._get_images_main()
    if self.tl_img and self.tv_img:
      self._match()
      self._prob()
    self._negative()
    return self.data

  def _load_prob(self, prob: str):
    resp = {
      f"{self.prefix}_prob_1": 0.0,
      f"{self.prefix}_prob_2": 0.0
    }
    if prob:
      prob = prob.replace(" ", "")
      parts = prob.split('=')

      if parts[0]:
        resp[f"{self.prefix}_prob_1"] = float(parts[0])
      else:
        resp[f"{self.prefix}_prob_1"] = 0.0

      if parts[1]:
        resp[f"{self.prefix}_prob_2"] = float(parts[1].replace("%", '')) / 100
      else:
        resp[f"{self.prefix}_prob_2"] = 0.0

      return resp

    return resp

  def _name(self):
    try:
      names = self.web_driver_util.get_elements_text(class_selector=".Box.Flex.ggRYVx.cRYpNI")
      if "tl" in self.prefix:
        self.data[f"{self.prefix}_nome"] = names[0]
      else:
        self.data[f"{self.prefix}_nome"] = names[1]
    except Exception as error:
      logger.error(error)

  def _gols(self):
    try:
      gols = self.web_driver_util.get_elements(class_selector=".Box.iCtkKe")
      gols = gols[0].replace(" ", "").split("-")
      if "tl" in self.prefix:
        self.data[f"{self.prefix}_gols"] = float(gols[0])
      else:
        self.data[f"{self.prefix}_gols"] = float(gols[1])
    except Exception as error:
      logger.error(error)

  def _odds(self):
    try:
      odds: list[str] = self.web_driver_util.get_elements_float(class_selector=".Box.Flex.lmZESE.fXkLii")
      new_data = {}
      if odds:
        for i in range(len(odds)):
          odds_split = odds[i].split("\n")
          if len(odds_split) == 6:
            if "tl" in self.prefix:
              new_data[f"{self.prefix}_odds_vitoria_{i}"] = float(odds_split[1]) * -1
              new_data[f"{self.prefix}_odds_empate_{i}"] = float(odds_split[3]) * -1
              new_data[f"{self.prefix}_odds_derrota_{i}"] = float(odds_split[5]) * -1
            else:
              odds_split = odds_split[::-1]
              new_data[f"{self.prefix}_odds_vitoria_{i}"] = float(odds_split[0]) * -1
              new_data[f"{self.prefix}_odds_empate_{i}"] = float(odds_split[2]) * -1
              new_data[f"{self.prefix}_odds_derrota_{i}"] = float(odds_split[4]) * -1

          self.data.update(new_data)
    except Exception as error:
      logger.error(f"erro download odds: {error}")

  def _opinion(self):
    try:
      opinion = self.web_driver_util.get_elements_float(class_selector=".Text.bGtKrY")
      if opinion:
        if "tv" in self.prefix:
          opinion = opinion[::-1]
        array_dict = {self._keys_opinion[i]: opinion[i] for i in range(len(opinion))}
        self.data.update(array_dict)
    except Exception as error:
      logger.error(f"erro download option: {error}")

  def _general_data(self):
    try:
      general_data = self.web_driver_util.get_elements(class_selector=".Box.Flex.heNsMA.bnpRyo")

      if general_data:
        new_data = {}
        if "tl" in self.prefix:
          for item in general_data:
            split = item.split("\n")
            new_data[f"{self.prefix}_{TextUtil.transform_text(split[1])}"] = TextUtil.convert_percentage(split[0])
        else:
          for item in general_data:
            split = item.split("\n")
            new_data[f"{self.prefix}_{TextUtil.transform_text(split[1])}"] = TextUtil.convert_percentage(split[2])

        self.data.update(new_data)
    except Exception as error:
      logger.error(error)

  def _match_team(self, index):
    result_match = self.web_driver_util.get_elements(class_selector=".Box.Flex.cBIkhT.jLRkRA")
    result_match_list = []

    for item in result_match:
      parts = item.split('\n')
      position = float(parts[0])
      pontos = float(parts[-1])
      partidas = parts[1:-1]
      dict_item = {
        "position": position,
        "pontos": pontos
      }
      for i in range(len(partidas)):
        dict_item[f"partida_{i}"] = partidas[i]

      result_match_list.append(dict_item)

    def change_tipo(key, value) -> any:
      if "partida" in key:
        if "E" == value:
          return 0
        elif "V" == value:
          return 1
        elif "D" == value:
          return 2
      return value

    self.data.update(
      {f"{self.prefix}_{key}": change_tipo(key, value) for key, value in result_match_list[index].items()})

  def _get_images_main(self):
    try:
      images_tl = self.web_driver_util.get_elements_img(class_selector=".Box.Flex.ddaRLc.jCkSpU")
      images_tv = self.web_driver_util.get_elements_img(class_selector=".Box.Flex.cntHhX.jCkSpU")
      if images_tl:
        self.tl_img = images_tl[0]
      if images_tv:
        self.tv_img = images_tv[0]
    except Exception as error:
      logger.error(error)

  def _get_prob(self, index):
    try:
      prob = self.web_driver_util.get_elements(class_selector=".Box.Flex.ggRYVx.bVOneM")
      self.data.update(self._load_prob(prob[index]))
    except Exception as e:
      logger.error(e)

  def _match(self):
    try:
      images = self.web_driver_util.get_elements_img(class_selector=".Box.Flex.kCMpjR.jCkSpU")
      self._match_team(images.index(self.tl_img) if "tl" in self.prefix else images.index(self.tv_img))
    except Exception as error:
      logger.error(f"error downloand image: {error}")

  def _prob(self):
    try:
      images_prob = self.web_driver_util.get_elements_img(class_selector=".Box.Flex.ggRYVx.bVOneM")
      index = images_prob.index(self.tl_img) if "tl" in self.prefix else images_prob.index(self.tv_img)
      self._get_prob(index)
    except Exception as error:
      logger.error(f"error downloand image: {error}")

  def _negative(self):
    try:
      self.data.update({key: self.data[key] * -1 for key in self._keys_general_data_negative if key in self.data})
    except Exception as error:
      logger.error(error)

  def close(self):
    self.web_driver_util.close()


if __name__ == '__main__':
  # https://www.sofascore.com/pt/santa-clara-b-sad/ekbslkb#id:8953898
  # https://www.sofascore.com/pt/criciuma-cruzeiro/eOsJO#id:12117113
  # https://www.sofascore.com/pt/gremio-fluminense/lOsBtc#id:12117094

  instance = DownloadTeamData(url="https://www.sofascore.com/pt/gremio-fluminense/lOsBtc#id:12117094")
  instance2 = DownloadTeamData(url="https://www.sofascore.com/pt/criciuma-cruzeiro/eOsJO#id:12117113")

  df = instance.download()
  df2 = instance2.download()
  instance2.close()
  instance.close()

  df_final = pd.concat([df, df2])

  df_final.to_csv("./datasets.csv", index=False)
  df_final.to_parquet("./datasets.parquet", index=False)
  print(df_final)
