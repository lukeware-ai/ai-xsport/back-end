import time
from datetime import date
from pathlib import Path

import pandas as pd

from bot_web.bot.download_team_data import DownloadTeamData
from log_config import setup_logger

logger = setup_logger(__name__)


class ProcessTeamData:
  FILE_NAME = 'dataset'

  def __init__(self, url: str, output_folder: str, date: date):
    self.url = url
    self.date = date
    self.output_file = output_folder
    self.download_team_data = None
    self.download_team_data = DownloadTeamData(self.url)

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_value, trace):
    self.download_team_data.close()

  def run(self, pbar=None) -> None:
    logger.info(f"Processing data for date: {self.date}")
    df = self.download_team_data.download()
    if not df.empty:
      df["date"] = self.date
      date_format = self.date.strftime('%Y_%m')
      date_format_ful = self.date.strftime('%Y_%m_%d')

      path = f"{self.output_file}/{date_format}"
      Path(path).mkdir(parents=True, exist_ok=True)

      folder_path = f"{path}/{ProcessTeamData.FILE_NAME}_{date_format_ful}"
      file_path = Path(f"{folder_path}.csv")
      if file_path.exists():
        df_old = pd.read_csv(file_path)
        df_new = pd.concat([df, df_old])
        df_new.to_csv(file_path, index=False)
      else:
        df.to_csv(file_path, index=False)

    if pbar:
      pbar.update(1)


if __name__ == '__main__':
  url = "https://www.sofascore.com/pt/club-sporting-cristal-sao-paulo/GOscW#id:9470695"
  with ProcessTeamData(url=url, output_folder="../../dataset", date=date.today()) as process_team_data:
    start = time.time()
    process_team_data.run()
    end = time.time()
    logger.warning(f"Execution Time: {end - start} secounds")
