import os
import re
from pathlib import Path
from urllib.parse import urlparse

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


class LinksUtil:

  @staticmethod
  def _save_links(link_main, sub_links: list[str]):
    folder_main = link_main.split("/")[-1].split("-")[:-2][0]
    folder_links = f"./links/{folder_main}"
    Path(folder_links).mkdir(parents=True, exist_ok=True)
    filename = LinksUtil._url_to_filename(link_main)
    file_path = os.path.join(folder_links, f"{filename}.txt")
    with open(file_path, "w") as f:
      f.writelines(sub_links)

  @staticmethod
  def _url_to_filename(url):
    parsed_url = urlparse(url)
    url_path = f"{parsed_url.netloc}{parsed_url.path}"
    return re.sub(r"[^a-zA-Z0-9]", "_", url_path)

  @staticmethod
  def _get_sub_links(link_main: str) -> list:
    driver: webdriver.Chrome
    try:
      options = webdriver.ChromeOptions()
      options.add_argument("--window-size=1800,1200")
      options.add_argument("--headless")
      options.add_argument("--incognito")
      driver = webdriver.Chrome(options=options)
      driver.get(link_main)
    except WebDriverException as e:
      raise RuntimeError(f"error running webdriver: {e}")

    data: list[str] = []
    try:
      elements = WebDriverWait(driver, 10).until(
        ec.presence_of_all_elements_located(
          (
            By.XPATH,
            "//a[starts-with(@class, 'sc-lcIPJg hFgRgD')]",
          )
        )
      )
      for el in elements:
        data.append(el.get_attribute("href"))

    except Exception as e:
      raise RuntimeError(f"Error Elements not found: {e}")

    return data

  @staticmethod
  def search_links(link_main: str) -> None:
    links: list[str] = LinksUtil._get_sub_links(link_main)

    sub_links: list[str] = []
    for link in links:
      if "#id" in link:
        sub_links.append(link + "\n")

    LinksUtil._save_links(link_main, sub_links)


if __name__ == '__main__':
  LinksUtil.search_links("https://www.sofascore.com/pt/futebol/2024-07-03")
