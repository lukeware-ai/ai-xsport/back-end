from pathlib import Path

import pandas as pd


class FileUtils:
  @staticmethod
  def get_files_txt(path: str) -> list[str]:
    directory = Path(path)
    files: list = []
    for file in directory.rglob("*.txt"):
      files.append(file)
    return files

  @staticmethod
  def import_data(origin: str, destination: str, filename: str) -> None:

    dfs: list[pd.DataFrame] = []

    folder_origin = Path(origin)
    folder_destination = Path(destination)
    folder_destination.mkdir(parents=True, exist_ok=True)

    line_error = []

    def log_error(line):
      line_error.append(line)

    for file in folder_origin.rglob("*.csv"):
      try:
        df: pd.DataFrame = pd.read_csv(str(file), on_bad_lines=log_error, engine='python')
        dfs.append(df)
      except Exception as error:
        print(error)

    if dfs:
      dataset = pd.concat(dfs, ignore_index=True)

      dataset_numeric = dataset.drop(columns=["tl_nome", "tv_nome", "resultado", "date"])
      for column in dataset_numeric.columns:
        dataset[column] = pd.to_numeric(dataset[column], errors='coerce')

      dataset = dataset.reindex(sorted(dataset.columns), axis=1)
      dataset.dropna(subset=['resultado'], inplace=True)
      allowed_results = ['empate', 'vitoria_time_local', 'vitoria_time_visitante']
      dataset = dataset.loc[dataset['resultado'].isin(allowed_results)]

      tl_count = sum([col.startswith('tl') for col in dataset.columns])
      tv_count = sum([col.startswith('tv') for col in dataset.columns])

      print(f"Colunas que começam com 'tl': {tl_count}")
      print(f"Colunas que começam com 'tv': {tv_count}")

      file_path = f"{str(folder_destination)}/{filename}.csv"
      dataset.to_csv(file_path, index=False)

      parquet_path = file_path.replace(".csv", ".parquet")
      dataset.to_parquet(parquet_path, engine="pyarrow", index=False)

    return None

  @staticmethod
  def delete(filename: str) -> None:
    file_path = Path(filename)
    if file_path.exists() and file_path.is_file():
      file_path.unlink()
