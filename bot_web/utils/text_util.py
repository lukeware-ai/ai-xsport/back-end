import re
import unicodedata


class TextUtil:
  @staticmethod
  def transform_text(text):
    new_text = text.lower()
    new_text = new_text.replace(" ", "_")
    new_text = unicodedata.normalize("NFKD", new_text).encode("ASCII", "ignore").decode("ASCII")
    new_text = re.sub(r"\W", "", new_text)
    return new_text

  @staticmethod
  def convert_percentage(value):
    if not value:
      return 0.0

    pattern_1 = r'^\d+%$'
    pattern_2 = r'\d+/\d+ \((\d+%)\)'

    match_1 = re.match(pattern_1, value)
    match_2 = re.match(pattern_2, value)

    if match_1:
      return float(value.strip('%')) / 100
    elif match_2:
      return float(match_2.group(1).strip('%')) / 100
    else:
      return float(value)
