from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from bot_web.utils.text_util import TextUtil


class WebDriverUtil:
  TIMEOUT = 12
  _driver = None

  def __init__(self, url: str):
    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1800,1200")
    options.add_argument("--headless")
    options.add_argument("--incognito")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-extensions")
    options.add_argument("--disable-plugins-discovery")
    self._driver = webdriver.Chrome(options=options)
    self._driver.set_page_load_timeout(60)
    self._driver.set_script_timeout(60)
    self._driver.get(url)

  def get_elements(self, class_selector: str):
    data = []
    try:
      elements = WebDriverWait(self._driver, self.TIMEOUT).until(
        ec.presence_of_all_elements_located((By.CSS_SELECTOR, class_selector))
      )
      for element in elements:
        data.append(element.text)
    except Exception as e:
      raise RuntimeError(f"error occurred: {e}")

    return data

  def get_elements_img(self, class_selector: str):
    data = []
    try:
      elements = WebDriverWait(self._driver, self.TIMEOUT).until(
        ec.presence_of_all_elements_located((By.CSS_SELECTOR, class_selector))
      )
      for element in elements:
        img_tag = element.find_element(By.TAG_NAME, "img")
        img_src = img_tag.get_attribute("src")
        data.append(img_src)
    except Exception as e:
      raise RuntimeError(f"error occurred {class_selector}: {e}")

    return data

  def get_elements_text(self, class_selector):
    data_list: list = self.get_elements(class_selector)
    return [TextUtil.transform_text(data) for data in data_list]

  def get_elements_float(self, class_selector):
    try:
      data_list: list = self.get_elements(class_selector)
      with_characters = [it for it in data_list if "%" in it]
      if with_characters:
        data_list = [TextUtil.convert_percentage(it) for it in data_list]
      return data_list
    except Exception as e:
      raise RuntimeError(f"error occurred {class_selector}: {e}")

  def close(self):
    if self._driver:
      self._driver.quit()
